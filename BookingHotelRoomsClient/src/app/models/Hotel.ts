export class Hotel {
    id: number;
    hotelName: string;
    rating: number;
    country: string;
    city: string;
    address: string;

    constructor() {}
}