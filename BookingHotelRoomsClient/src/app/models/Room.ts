import { Hotel } from './Hotel';
import { RoomStatus } from './RoomStatus';

export class Room {
    id: number;
    roomTitle: string;
    adultsMax: number;
    childrenMax: number;
    aboutRoom: string;
    pricePerNight: number;
    roomStatus: RoomStatus;
    hotel: Hotel;

    constructor() {}

}