import { Role } from './Role';

export class User {
    id: number;
    firstName: string;
    lastName: string;
    email: string;
    username: string;
    password: string;
    last_update_date: string;
    created_date: string;
    deleted_date: string;
    //token: string;
    role: Role;

    constructor() {}
}