import { Component, OnInit } from '@angular/core';
import { Reservation } from 'src/app/models/Reservation';
import { ReservationService } from 'src/app/services/reservation.service';
import { User } from 'src/app/models/User';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.css']
})
export class ReservationsComponent implements OnInit {

  reservations: Array<Reservation> = [];

  constructor(private reservationService: ReservationService) { }

  ngOnInit() {
    this.loadReservation();
  }

  private loadReservation() {
    this.reservationService.getAllReservationsByUser(localStorage.getItem('id')).subscribe
    (
      (res: Reservation[]) => {
        this.reservations = res;
      },
      error => {
        alert("Error has occured while getting reservations");
      }
    )
  }

  cancelReservation(reservationId: number) {

    // Delete from UI
    this.reservations = this.reservations.filter(reservation => reservation.id != reservationId);
    //Delete from server
    this.reservationService.cancelReservation(reservationId).subscribe
    (
      (res: Reservation) => {
        //alert('Reservation have successfully canceled');
      },
      error => {
        alert("Error has occured while canceling reservation");
      }
    )
  }

}
