import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  title: string = "Online booking";

  constructor(private authService: AuthenticationService,
              private router: Router
  ) {}

  ngOnInit() {}

  logout() {
    //this.authService.logout();
    //this.router.navigate(["/signIn"]);
  }

}
