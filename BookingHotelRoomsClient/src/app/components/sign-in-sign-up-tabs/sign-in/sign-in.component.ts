import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/authentication.service';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  title: string = "Sign In";
  signInForm: FormGroup;
  submitted = false;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private authenticationService: AuthenticationService 
  ) {

    // redirect to home if already logged in
    if(authenticationService.currentUser()) {
      this.router.navigate(['']);
    }

  }

  ngOnInit() {
    this.signInForm = this.formBuilder.group({
      email: ['', Validators.required],
      password: ['', Validators.required]
    });
  }

  // Convenience getter for easy access to form field
  get f() { return this.signInForm.controls }

  signIn(){
    this.submitted = true;

    // Stop here if form is invalid
    if(this.signInForm.invalid) {
      return;
    }

    this.authenticationService.signIn(this.f.email.value, this.f.password.value).subscribe
    (
      (res) => {
        //localStorage.setItem("token",res.token);
        localStorage.setItem("id",String(res.id));
        localStorage.setItem("email",res.email);
        localStorage.setItem("role", res.role.roleName);
        localStorage.setItem("firstName",res.firstName);
        localStorage.setItem("lastName",res.lastName);
        //this.alertService.success("You have successfully logged in");
        this.router.navigate(['/home']);
      },
      error => {
        //this.alertService.error(error.error);
        alert("Email or password is invalid");
      }
    )
  }


}
