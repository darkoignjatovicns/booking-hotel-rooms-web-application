import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-room-filter',
  templateUrl: './room-filter.component.html',
  styleUrls: ['./room-filter.component.css']
})
export class RoomFilterComponent implements OnInit {

  @Output() filterRooms: EventEmitter<any> = new EventEmitter();

  roomsFilterForm: FormGroup;
  submitted: boolean = false;

  /*country: string = '';
  city: string = '';
  rating: number = 0;
  pricePerNight: number = 0;
  adults: number = 1;
  children: number = -1;
  checkIn: Date = null;
  checkOut: Date =  null;*/

  

  constructor(private formBuilder: FormBuilder) { }

  

  ngOnInit() {
    this.roomsFilterForm = this.formBuilder.group({
      city: ['',Validators.required],
      adults: [2, Validators.required],
      children: [0, Validators.required],
      checkIn:['', Validators.required],
      checkOut:['', Validators.required]
    });
  }

  // Convenience getter for easy access form fields 
  get f() { return this.roomsFilterForm.controls; }


  onSubmit() {
   // console.log("Test1");
    this.submitted = true;

    // Stop here if form is invalid
    if(this.roomsFilterForm.invalid) {
    //  console.log("Test2");
      return;
    }
   // console.log("Test3");
    /*console.log(this.f.city.value);
    console.log(this.f.adults.value);
    console.log(this.f.children.value);
    console.log(this.f.checkIn.value);
    console.log(this.f.checkOut.value);*/

    const filterFields = { 
      city: this.f.city.value,
      adults: this.f.adults.value,
      children: this.f.children.value,
      checkIn: this.f.checkIn.value,
      checkOut: this.f.checkOut.value
    }
   /* const filterFields = {
      country: this.country,
      city: this.city,
      rating: this.rating,
      pricePerNight: this.pricePerNight,
      adults: this.adults,
      children: this.children,
      checkIn: this.checkIn,
      checkOut: this.checkOut
    }*/
   
    this.filterRooms.emit(filterFields);
  }

}
