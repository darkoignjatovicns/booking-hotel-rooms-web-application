import { Component, OnInit } from '@angular/core';
import { Room } from 'src/app/models/Room';
import { RoomService } from 'src/app/services/room.service';
import { ReservationService } from 'src/app/services/reservation.service';
import { Router } from '@angular/router';
import { Reservation } from 'src/app/models/Reservation';


@Component({
  selector: 'app-rooms',
  templateUrl: './rooms.component.html',
  styleUrls: ['./rooms.component.css']
})
export class RoomsComponent implements OnInit {

  //rooms: Room[] = [];
  filteredRooms: Room[] = [];
  checkInDate: Date = null;
  checkOutDate: Date = null;
  searchTerm: string;
  //private _searchTerm: string;

 /* get searchTerm(): string {
    return this._searchTerm;
  }

  set searchTerm(value: string) {
    this._searchTerm = value;
    this.filteredRooms = this.searchFilter(value); 
  }

  searchFilter(searchString: string) {
    //console.log(searchString);
    return this.filteredRooms.filter(room => 
       // room.hotel.hotelName.toLowerCase().indexOf(searchString.toLowerCase()) !== -1 );
       room.hotel.hotelName.includes("searchString"));
  }*/


  constructor(private roomService: RoomService,
              private reservationService: ReservationService,
              private router: Router 
  ) { }

  ngOnInit() {
   // this.loadRooms();
  }

 

  filterRooms(inputFields: any) {

    this.roomService.getFilteredRooms(inputFields).subscribe
    (
      (res: Room[]) => {
        this.filteredRooms = res;
        this.checkInDate = inputFields.checkIn;
        this.checkOutDate = inputFields.checkOut;
      },
      error => {
        alert("Error has occured while getting rooms");
      }
    )

   /* if(inputFields.city != '') {
      this.filteredRooms = this.filteredRooms.filter(r => r.hotel.city == inputFields.city);
    } 
    if( inputFields.adultsMax != 1) {
      this.filteredRooms = this.filteredRooms.filter(r => r.adultsMax >= inputFields.adults);
    } 
    if(inputFields.children != -1) {
      this.filteredRooms = this.filteredRooms.filter(r => r.childrenMax >= inputFields.children);
    }*/
  }

 /* loadRooms() {
    this.roomService.getAllRooms().subscribe
    (
      (res) => {
        this.rooms = res;
        this.filteredRooms = res;
      },
      error => {
        alert("Error has been occured while getting all rooms");
      }
    )
  }*/

  reserveRoom(roomId: number,checkInDate: Date, checkOutDate: Date) {

    this.reservationService.reserveRoom(localStorage.getItem('id'), String(roomId), this.checkInDate, this.checkOutDate).subscribe
    (
      (res: Reservation) => {
        this.router.navigate(['/reservations']);
      },
      error => {
        alert("Error has occured while making reservation");
      }
    )
  }

}
