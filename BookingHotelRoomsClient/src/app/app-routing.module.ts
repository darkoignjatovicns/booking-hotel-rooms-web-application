import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './components/home/home.component';
import { SignUpComponent } from './components/sign-in-sign-up-tabs/sign-up/sign-up.component';
import { SignInSignUpTabsComponent } from './components/sign-in-sign-up-tabs/sign-in-sign-up-tabs.component';
import { SignInComponent } from './components/sign-in-sign-up-tabs/sign-in/sign-in.component';
import { AuthGuard } from './guards/auth.guard';
import { RoomsComponent } from './components/rooms/rooms.component';
import { LogoutComponent } from './components/logout/logout.component';
import { ReservationsComponent } from './components/reservations/reservations.component';

const routes: Routes = [
  
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { 
    path: 'signIn', component: SignInSignUpTabsComponent,
    children: [{ path: '', component: SignInComponent }]      
  },
  {
    path: 'signUp', component: SignInSignUpTabsComponent,
    children: [{ path: '', component: SignUpComponent }]
  },
  { path: 'home', component: HomeComponent,canActivate: [AuthGuard] },
  { path: 'rooms', component: RoomsComponent, canActivate: [AuthGuard] },
  { path: 'logout', component: LogoutComponent, canActivate: [AuthGuard]}, 
  { path: 'reservations', component: ReservationsComponent, canActivate: [AuthGuard]},
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
