import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';


import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { NavigationComponent } from './components/layout/navigation/navigation.component';
import { SignUpComponent } from './components/sign-in-sign-up-tabs/sign-up/sign-up.component';
import { SignInSignUpTabsComponent } from './components/sign-in-sign-up-tabs/sign-in-sign-up-tabs.component';
import { SignInComponent } from './components/sign-in-sign-up-tabs/sign-in/sign-in.component';
import { RoomsComponent } from './components/rooms/rooms.component';
import { LogoutComponent } from './components/logout/logout.component';
import { RoomFilterComponent } from './components/rooms/room-filter/room-filter.component';
import { RoomItemComponent } from './components/rooms/room-item/room-item.component';
import { ReservationsComponent } from './components/reservations/reservations.component';
import { RoomSearchTermFilterPipe } from './shared/room-search-term-filter.pipe';


@NgModule({
  declarations: [
    AppComponent,
    SignInComponent,
    HomeComponent,
    NavigationComponent,
    SignUpComponent,
    SignInSignUpTabsComponent,
    RoomsComponent,
    LogoutComponent,
    RoomFilterComponent,
    RoomItemComponent,
    ReservationsComponent,
    RoomSearchTermFilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
