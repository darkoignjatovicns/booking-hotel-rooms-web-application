import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Reservation } from '../models/Reservation';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  }) 
}

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  private BASE_URL: string = 'http://localhost:8080';
  private GET_ALL_RESERVATIONS_BY_USER = `${this.BASE_URL}\\reservations\\getAllReservationsByUser`;
  private RESERVE_ROOM_URL = `${this.BASE_URL}\\reservations\\reserveRoom`;
  private CANCEL_RESERVATION_URL = `${this.BASE_URL}\\reservations\\cancelReservation`;

  constructor(private http: HttpClient) { }

  public getAllReservationsByUser(userId: string): Observable<Reservation[]> {

    let headers = new HttpHeaders();
    headers.append('Content-Type','application/json');

    let params = new HttpParams();
    params = params.set('userId', userId);
    return this.http.get<Reservation[]>(this.GET_ALL_RESERVATIONS_BY_USER, { 'headers': headers, 'params': params });
  }

  public reserveRoom(userId: string, roomId: string, checkInDate: Date, checkOutDate: Date) : Observable<Reservation> {

    return this.http.post<Reservation>(this.RESERVE_ROOM_URL,
          { 'userId': userId, 'roomId': roomId, 'checkInDate': checkInDate, 'checkOutDate': checkOutDate },
          httpOptions);

  }

  public cancelReservation(reservationId: number) : Observable<Reservation> {
    //return this.http.put<Reservation>(this.CANCEL_RESERVATION_URL,{ 'reservationId': reservationId },httpOptions);
    return this.http.put<Reservation>(this.CANCEL_RESERVATION_URL, reservationId, httpOptions);
  } 

}
