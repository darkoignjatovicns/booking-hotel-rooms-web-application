import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/User';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  private BASE_URL = 'http://localhost:8080';
  private SING_IN_URL = `${this.BASE_URL}\\authentication\\signIn`;

  constructor(private http: HttpClient) { }

  signIn(email: string, password: string) : Observable<User> {
    //return this.http.post<User>(this.SING_IN_URL,{ 'email': email, 'password': password });

    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    let params = new HttpParams();
    params = params.set("email", email);
    params = params.set("password", password);

    return this.http.get<User>(this.SING_IN_URL,{ 'headers': headers, 'params': params });
  }

  logout() {
    
    localStorage.removeItem("id");
    localStorage.removeItem("email");
    localStorage.removeItem("role");
    localStorage.removeItem("firstName");
    localStorage.removeItem("lastName");
    //localStorage.removeItem("token");
  }

  currentUser(role?: string): boolean {
    
    // Check if local storage is empty
    let roleNotEmpty: boolean = localStorage.getItem('role') != null;

    // If not,check is role set
    if(roleNotEmpty && role != null) {
      return localStorage.getItem('role') === role;
    }
    
    return roleNotEmpty;
  }


}
