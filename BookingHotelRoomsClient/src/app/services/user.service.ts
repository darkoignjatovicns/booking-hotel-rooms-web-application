import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
import { User } from '../models/User';
import { Observable } from 'rxjs';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json'
  })
}

@Injectable({
  providedIn: 'root'
})
export class UserService {


  private BASE_URL = 'http://localhost:8080';
  private SIGN_UP_URL = `${this.BASE_URL}\\users\\signUp`;

  constructor(private http: HttpClient) { }

  public signUp(user: User) : Observable<User> {
    return this.http.post<User>(this.SIGN_UP_URL,user,httpOptions);
  }
}
