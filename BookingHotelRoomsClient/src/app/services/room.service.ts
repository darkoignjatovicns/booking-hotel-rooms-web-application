import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Room } from '../models/Room';
import { HttpClient, HttpParams, HttpHeaderResponse, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  private BASE_URL: string = "http://localhost:8080";
  private GET_ALL_ROOMS_URL: string = `${this.BASE_URL}\\rooms\\getAllRooms`;
  private GET_FILTERED_ROOMS_URL: string = `${this.BASE_URL}\\rooms\\getFilteredRooms`;

  constructor(private http: HttpClient) { }

  public getAllRooms(): Observable<Room[]> {
    return this.http.get<Room[]>(this.GET_ALL_ROOMS_URL);
  }

  public getFilteredRooms(inputFields) :Observable<Room[]> {

    let headers = new HttpHeaders();
    headers.append("Content-Type","application/json");

    let params = new HttpParams();
    params = params.set("city", inputFields.city);
    params = params.set("adults", inputFields.adults);
    params = params.set("children", inputFields.children);
    params = params.set("checkIn", inputFields.checkIn);
    params = params.set("checkOut", inputFields.checkOut);

    return this.http.get<Room[]>(this.GET_FILTERED_ROOMS_URL, { 'headers': headers, 'params' :params });
  }



}
