CREATE DATABASE  IF NOT EXISTS `booking_hotel_rooms_world_db` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `booking_hotel_rooms_world_db`;
-- MySQL dump 10.13  Distrib 5.6.13, for Win32 (x86)
--
-- Host: 127.0.0.1    Database: booking_hotel_rooms_world_db
-- ------------------------------------------------------
-- Server version	5.5.61

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `hotel`
--

DROP TABLE IF EXISTS `hotel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hotel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_name` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `rating` double DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `last_update_date` date DEFAULT NULL,
  `deleted_date` date DEFAULT NULL,
  `image` longblob,
  `imagePut` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hotel`
--

LOCK TABLES `hotel` WRITE;
/*!40000 ALTER TABLE `hotel` DISABLE KEYS */;
INSERT INTO `hotel` VALUES (1,'Hotel Aleksandar','Serbia','Novi Sad','Cara Lazara 79',4,NULL,NULL,NULL,NULL,NULL),(2,'Hotel Park','Serbia','Novi Sad','Novosadska 35',4,NULL,NULL,NULL,NULL,NULL),(3,'Hotel Sheraton','Serbia','Novi Sad','Polgar Andraša 1',5,NULL,NULL,NULL,NULL,NULL),(4,'Hilton','Serbia','Belgrade','Kralja Milana 35',5,NULL,NULL,NULL,NULL,NULL),(5,'Clayton Hotel City','England','London','Haverstock Hill 8',5,NULL,NULL,NULL,NULL,NULL),(6,'Camden Enterprise','England','London','10 New Drum',5,NULL,NULL,NULL,NULL,NULL),(7,'Dorsett Hotel','England','London','Aldgate High 9',5,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `hotel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `message`
--

DROP TABLE IF EXISTS `message`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `message` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id_send` int(11) DEFAULT NULL,
  `user_id_receive` int(11) DEFAULT NULL,
  `message` text,
  `created_date` date DEFAULT NULL,
  `deleted_date` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_message_user1_idx` (`user_id_send`),
  KEY `fk_message_user2_idx` (`user_id_receive`),
  CONSTRAINT `fk_message_user1` FOREIGN KEY (`user_id_send`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_message_user2` FOREIGN KEY (`user_id_receive`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `message`
--

LOCK TABLES `message` WRITE;
/*!40000 ALTER TABLE `message` DISABLE KEYS */;
/*!40000 ALTER TABLE `message` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reservation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `room_id` int(11) DEFAULT NULL,
  `reservation_date` date DEFAULT NULL,
  `check_in_date` date DEFAULT NULL,
  `check_out_date` date DEFAULT NULL,
  `canceled_reservation_date` date DEFAULT NULL,
  `total_price` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_booking_user1_idx` (`user_id`),
  KEY `fk_booking_room1_idx` (`room_id`),
  CONSTRAINT `fk_booking_room1` FOREIGN KEY (`room_id`) REFERENCES `room` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_booking_user1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reservation`
--

LOCK TABLES `reservation` WRITE;
/*!40000 ALTER TABLE `reservation` DISABLE KEYS */;
INSERT INTO `reservation` VALUES (28,4,4,'2019-06-11','2019-06-11','2019-06-13','2019-06-11',0),(29,4,9,'2019-06-12','2019-06-11','2019-06-13','2019-06-12',0),(30,5,4,'2019-06-12','2019-06-11','2019-06-13','2019-06-12',0);
/*!40000 ALTER TABLE `reservation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` VALUES (1,'User'),(2,'Menager');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room`
--

DROP TABLE IF EXISTS `room`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `hotel_id` int(11) DEFAULT NULL,
  `room_status_id` int(11) DEFAULT '1',
  `room_title` varchar(45) DEFAULT NULL,
  `adults_max` int(11) DEFAULT NULL,
  `children_max` int(11) DEFAULT NULL,
  `about_room` text,
  `price_per_night` int(11) DEFAULT NULL,
  `last_update_date` date DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `image` longblob,
  `imagePut` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_room_room_status1_idx` (`room_status_id`),
  KEY `fk_room_hotel1_idx` (`hotel_id`),
  CONSTRAINT `fk_room_hotel1` FOREIGN KEY (`hotel_id`) REFERENCES `hotel` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_room_room_status1` FOREIGN KEY (`room_status_id`) REFERENCES `room_status` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room`
--

LOCK TABLES `room` WRITE;
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
INSERT INTO `room` VALUES (1,1,1,'Dvokrevetna soba',2,0,NULL,35,NULL,NULL,NULL,NULL),(2,1,1,'Trokrevetna soba',3,2,NULL,100,NULL,NULL,NULL,NULL),(3,2,1,'Cetvorokrevetna soba',2,2,NULL,45,NULL,NULL,NULL,NULL),(4,3,1,'Petorokrevetna soba',3,2,NULL,100,NULL,NULL,NULL,NULL),(5,4,1,'Cevorokrevetna soba',2,2,NULL,120,NULL,NULL,NULL,NULL),(6,5,1,'three bad room',2,0,NULL,300,NULL,NULL,NULL,NULL),(7,5,1,'four bad room',4,2,NULL,420,NULL,NULL,NULL,NULL),(8,6,1,'three bed room',3,3,NULL,370,NULL,NULL,NULL,NULL),(9,6,1,'two bad room',2,2,NULL,260,NULL,NULL,NULL,NULL),(10,6,1,'four bad room',4,2,NULL,480,NULL,NULL,NULL,NULL),(11,7,1,'two bad room ',2,2,NULL,900,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `room` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `room_status`
--

DROP TABLE IF EXISTS `room_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `room_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `room_status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `room_status`
--

LOCK TABLES `room_status` WRITE;
/*!40000 ALTER TABLE `room_status` DISABLE KEYS */;
INSERT INTO `room_status` VALUES (1,'FREE'),(2,'BUSY');
/*!40000 ALTER TABLE `room_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT '1',
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `last_update_date` date DEFAULT NULL,
  `deleted_date` date DEFAULT NULL,
  `image` longblob,
  `imagePut` varchar(1024) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_user_role_idx` (`role_id`),
  CONSTRAINT `fk_user_role` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,1,'user','user','user','user','user@gmail.com',NULL,NULL,NULL,NULL,NULL),(2,1,'Darko','Ignjatovic','dark021','Dark021.','darko@gmail.com','2019-06-11',NULL,NULL,NULL,NULL),(3,1,'Peaky','Blinders','peaky','Peaky021.','peaky@gmail.com','2019-06-11',NULL,NULL,NULL,NULL),(4,1,'Peaky','Blinders','peaky','Peakz021.','blinders@gmail.com','2019-06-11',NULL,NULL,NULL,NULL),(5,1,'Milan','Jovic','milan','Milan021.','milan@gmail.com','2019-06-12',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-06-13  9:20:51
