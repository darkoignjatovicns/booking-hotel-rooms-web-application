package bookingHotelRooms.service;

import java.util.List;

import bookingHotelRooms.dto.ReservationDTO;
import bookingHotelRooms.model.Reservation;


public interface ReservationService {
	
	public Reservation makeReservation(ReservationDTO reservationDto);
	public Reservation cancelReservation(int reservationId);
	public List<Reservation> getAllReservationsByUser(int reservationId);
	public List<Reservation> getAllReservations();
	
}
