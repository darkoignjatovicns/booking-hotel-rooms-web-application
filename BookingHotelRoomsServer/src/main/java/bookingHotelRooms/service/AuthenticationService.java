package bookingHotelRooms.service;

import org.springframework.stereotype.Service;

import bookingHotelRooms.model.User;

public interface AuthenticationService {
	
	public User signIn(String email, String password);
	
}
