package bookingHotelRooms.service;

import java.util.Date;
import java.util.List;

import bookingHotelRooms.model.Room;


public interface RoomService {
	
	public List<Room> getAllRooms();
	public List<Room> getFilteredRooms(String city, int adults, int children, Date checkIn, Date checkOut);
	
}
