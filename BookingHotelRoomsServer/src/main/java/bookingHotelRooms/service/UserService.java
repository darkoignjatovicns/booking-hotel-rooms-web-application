package bookingHotelRooms.service;

import bookingHotelRooms.dto.UserDTO;
import bookingHotelRooms.model.User;

public interface UserService {
	
	public User signUp(UserDTO userDto);
	
}
