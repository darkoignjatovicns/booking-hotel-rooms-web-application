package bookingHotelRooms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bookingHotelRooms.model.User;
import bookingHotelRooms.repository.AuthenticationRepository;
import bookingHotelRooms.service.AuthenticationService;

@Service
public class AuthenticationServiceImpl implements AuthenticationService {

	private final AuthenticationRepository authRepository;
	
	@Autowired
	public AuthenticationServiceImpl(AuthenticationRepository authRepository) {
		this.authRepository = authRepository;
	} 
	
	@Override
	public User signIn(String email, String password) {
		return authRepository.signIn(email, password);
	}
	
	
}
