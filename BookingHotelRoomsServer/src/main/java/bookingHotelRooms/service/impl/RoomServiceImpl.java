package bookingHotelRooms.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bookingHotelRooms.model.Room;
import bookingHotelRooms.repository.RoomRepository;
import bookingHotelRooms.service.RoomService;

@Service
public class RoomServiceImpl implements RoomService {
	
	private final RoomRepository roomRepository;
	
	@Autowired
	public RoomServiceImpl(RoomRepository roomRepository)  {
		this.roomRepository = roomRepository;
	}
	
	@Override
	public List<Room> getAllRooms() {
		return roomRepository.findAll();
	}


	@Override
	public List<Room> getFilteredRooms(String city, int adults, int children, Date checkIn, Date checkOut) {
		return roomRepository.getFilteredRooms(city, adults, children, checkIn, checkOut);
	}
	
	

}
