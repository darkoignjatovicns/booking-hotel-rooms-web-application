package bookingHotelRooms.service.impl;



import java.util.List;

import org.springframework.stereotype.Service;

import bookingHotelRooms.dto.ReservationDTO;
import bookingHotelRooms.model.Reservation;
import bookingHotelRooms.repository.ReservationRepository;
import bookingHotelRooms.service.ReservationService;

@Service
public class ReservationServiceImpl implements ReservationService {

	private final ReservationRepository reservationRepository;
		
	public ReservationServiceImpl(ReservationRepository reservationRepository) {
		this.reservationRepository = reservationRepository;
	}
	
	@Override
	public Reservation makeReservation(ReservationDTO reservationDto) {
		return reservationRepository.makeReservation(reservationDto);
	}

	@Override
	public Reservation cancelReservation(int reservationId) {
		return reservationRepository.cancelReservation(reservationId);		
	}

	@Override
	public List<Reservation> getAllReservationsByUser(int userId) {
		return reservationRepository.getAllReservationsByUser(userId);
	}

	@Override
	public List<Reservation> getAllReservations() {
		return reservationRepository.findAll();
	}
	
	
	
}
