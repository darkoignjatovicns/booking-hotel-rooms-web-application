package bookingHotelRooms.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import bookingHotelRooms.dto.UserDTO;
import bookingHotelRooms.model.User;
import bookingHotelRooms.repository.UserRepository;
import bookingHotelRooms.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;
	
	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public User signUp(UserDTO userDto) {
		return userRepository.signUp(userDto);
	}


	
	
	
	
}
