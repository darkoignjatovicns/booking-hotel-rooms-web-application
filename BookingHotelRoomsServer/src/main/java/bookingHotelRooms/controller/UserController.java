package bookingHotelRooms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import bookingHotelRooms.dto.UserDTO;
import bookingHotelRooms.model.User;
import bookingHotelRooms.service.UserService;

@RequestMapping(value="users")
@RestController
@CrossOrigin()
public class UserController {
	
	private final UserService userService;
	
	@Autowired
	public UserController(UserService userService) {
		this.userService = userService;
	}
	
	@PostMapping(path="/signUp", produces=MediaType.APPLICATION_JSON_VALUE)
	public User signUp(@RequestBody UserDTO userDto) {
		return userService.signUp(userDto);
	}
	
}
