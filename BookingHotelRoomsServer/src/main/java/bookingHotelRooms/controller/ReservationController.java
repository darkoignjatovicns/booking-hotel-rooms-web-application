package bookingHotelRooms.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bookingHotelRooms.dto.ReservationDTO;
import bookingHotelRooms.model.Reservation;
import bookingHotelRooms.service.ReservationService;

@RestController
@RequestMapping("reservations")
@CrossOrigin()
public class ReservationController {
	
	private final ReservationService reservationService;
	
	@Autowired
	public ReservationController(ReservationService reservationService) {
		this.reservationService = reservationService;
	}
	
	@PostMapping(path="/reserveRoom", produces=MediaType.APPLICATION_JSON_VALUE)
	public Reservation makeReservation(@RequestBody ReservationDTO reservationDto) {
		return reservationService.makeReservation(reservationDto);
	}
	
	@PutMapping(path="/cancelReservation", produces=MediaType.APPLICATION_JSON_VALUE)
	public Reservation cancelReservation(@RequestBody int reservationId) {
		return reservationService.cancelReservation(reservationId);
	}
	
	@GetMapping(path="/getAllReservationsByUser", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Reservation> getAllReservationsByUser(@RequestParam("userId") int userId) {
		return reservationService.getAllReservationsByUser(userId);
	}
	
	@GetMapping(path="/getAllReservations", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Reservation> getAllReservations() {
		return reservationService.getAllReservations();
	}
}
