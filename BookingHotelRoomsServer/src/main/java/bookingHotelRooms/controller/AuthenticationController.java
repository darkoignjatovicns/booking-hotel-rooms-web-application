package bookingHotelRooms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bookingHotelRooms.model.User;
import bookingHotelRooms.service.AuthenticationService;

@RequestMapping("/authentication")
@RestController
@CrossOrigin()
public class AuthenticationController {
	
	private final AuthenticationService authService;
	
	@Autowired
	public AuthenticationController(AuthenticationService authService) {
		this.authService = authService;
	}
	
	@GetMapping(path="/signIn", produces=MediaType.APPLICATION_JSON_VALUE)
	public User signIn(@RequestParam("email") String email, @RequestParam("password") String password) {
		System.out.println(email + " " + password);
		
		User user = authService.signIn(email, password);
		System.out.print(user.getEmail());
		return user;
		
	}
	
}
