package bookingHotelRooms.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import bookingHotelRooms.model.Room;
import bookingHotelRooms.service.RoomService;

@RequestMapping("rooms")
@RestController
@CrossOrigin()
public class RoomController {
	
	private final RoomService roomService;
	
	@Autowired
	public RoomController(RoomService roomService) {
		this.roomService = roomService;
	}
	
	@GetMapping(path="/getAllRooms", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Room> getAllRooms() {
		return roomService.getAllRooms();
	}
	
	@GetMapping(path="/getFilteredRooms", produces=MediaType.APPLICATION_JSON_VALUE)
	public List<Room> getFilteredRooms(@RequestParam("city") String city,
									   @RequestParam("adults") String adults,
									   @RequestParam("children") String children,
									   @RequestParam("checkIn") String checkIn,
									   @RequestParam("checkOut") String checkOut
	) {
	
	int parseAdults = Integer.parseInt(adults);
	int parseChildren = Integer.parseInt(children);
	Date parseCheckIn = null;
	Date parseCheckOut= null;
	SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	try {
		parseCheckIn = sdf.parse(checkIn);
		parseCheckOut = sdf.parse(checkOut);
		
	}catch(Exception ex) {
		ex.printStackTrace();
	}
	
	return roomService.getFilteredRooms(city, parseAdults, parseChildren, parseCheckIn, parseCheckOut);
	}
	
	
}