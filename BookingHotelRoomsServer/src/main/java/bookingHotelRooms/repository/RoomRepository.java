package bookingHotelRooms.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bookingHotelRooms.model.Room;

@Repository
public interface RoomRepository extends JpaRepository<Room,Integer> {
	
	public List<Room> findAll();
	
	@Query("SELECT r FROM Room r WHERE r.hotel.city LIKE :city AND r.adultsMax >= :adults AND r.childrenMax >= :children"
			+ " AND NOT EXISTS (SELECT res FROM Reservation res WHERE res.room = r"
			+ " AND (:checkIn BETWEEN res.checkInDate AND res.checkOutDate OR :checkOut BETWEEN res.checkInDate AND res.checkOutDate)"
			+ " AND res.canceledReservationDate = NULL)")
	public List<Room> getFilteredRooms(@Param("city") String city,
									   @Param("adults") int adults,
									   @Param("children") int children,
									   @Param("checkIn") Date checkIn,
									   @Param("checkOut") Date checkOut
	);
	
	
}
