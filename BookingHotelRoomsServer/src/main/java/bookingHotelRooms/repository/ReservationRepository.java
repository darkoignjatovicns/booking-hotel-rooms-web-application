package bookingHotelRooms.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bookingHotelRooms.model.Reservation;
import bookingHotelRooms.model.User;
import bookingHotelRooms.repository.custom.CustomReservationRepository;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Integer>, CustomReservationRepository {
	
	List<Reservation> findAll();
	
	@Query("SELECT r FROM Reservation r WHERE r.user.id = :userId AND r.canceledReservationDate = NULL")
	public List<Reservation> getAllReservationsByUser(@Param("userId") int userId);
	
	
}
