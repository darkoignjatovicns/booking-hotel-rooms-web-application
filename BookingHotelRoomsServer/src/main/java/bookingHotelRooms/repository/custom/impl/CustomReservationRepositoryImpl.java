package bookingHotelRooms.repository.custom.impl;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import bookingHotelRooms.dto.ReservationDTO;
import bookingHotelRooms.model.Reservation;
import bookingHotelRooms.model.Room;
import bookingHotelRooms.model.RoomStatus;
import bookingHotelRooms.model.User;
import bookingHotelRooms.repository.custom.CustomReservationRepository;

@Repository
@Transactional
public class CustomReservationRepositoryImpl implements CustomReservationRepository {

	@PersistenceContext
	EntityManager em;
	
	@Override
	public Reservation makeReservation(ReservationDTO reservationDto) {
		Reservation reservation = new Reservation();
		reservation.setReservationDate(new Date());
		reservation.setCheckInDate(reservationDto.getCheckInDate());
		reservation.setCheckOutDate(reservationDto.getCheckOutDate());
		
		User user = em.find(User.class, reservationDto.getUserId());
		Room room = em.find(Room.class, reservationDto.getRoomId());
		//RoomStatus roomStatus = em.find(RoomStatus.class, 2); // BUSY
		//room.setRoomStatus(roomStatus);
		
		
		
		reservation.setRoom(room);
		reservation.setUser(user);
		
		em.persist(reservation);
		return reservation;
		
	}

	@Override
	public Reservation cancelReservation(int reservationId) {
		Reservation reservation = em.find(Reservation.class, reservationId);
		reservation.setCanceledReservationDate(new Date());
		
		RoomStatus roomStatus = em.find(RoomStatus.class, 1); // FREE
		Room room = em.find(Room.class,reservation.getRoom().getId());
		room.setRoomStatus(roomStatus);
		
		em.merge(reservation);
		return reservation;
	}
	
	
	
}
