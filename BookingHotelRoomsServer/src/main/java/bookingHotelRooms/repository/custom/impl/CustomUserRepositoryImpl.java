package bookingHotelRooms.repository.custom.impl;

import java.util.Date;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import bookingHotelRooms.dto.UserDTO;
import bookingHotelRooms.model.Role;
import bookingHotelRooms.model.User;
import bookingHotelRooms.repository.custom.CustomUserRepository;

@Repository
@Transactional
public class CustomUserRepositoryImpl implements CustomUserRepository {
	
	@PersistenceContext
	EntityManager em;

	@Override
	public User signUp(UserDTO userDto) {
		
		User user = new User();
		user.setFirstName(userDto.getFirstName());
		user.setLastName(userDto.getLastName());
		user.setEmail(userDto.getEmail());
		user.setUsername(userDto.getUsername());
		user.setPassword(userDto.getPassword());
		user.setCreatedDate(new Date());
		
		Role role = em.find(Role.class,1);
		user.setRole(role);
		
		em.persist(user);
		return user;
		
	}
	
	
	
	
}
