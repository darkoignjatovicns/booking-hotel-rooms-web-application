package bookingHotelRooms.repository.custom;

import bookingHotelRooms.dto.UserDTO;
import bookingHotelRooms.model.User;

public interface CustomUserRepository {
	
	public User signUp(UserDTO userDto);
	
}
