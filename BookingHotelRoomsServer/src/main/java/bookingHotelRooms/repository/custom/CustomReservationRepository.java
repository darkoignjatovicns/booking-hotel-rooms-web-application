package bookingHotelRooms.repository.custom;

import bookingHotelRooms.dto.ReservationDTO;
import bookingHotelRooms.model.Reservation;

public interface CustomReservationRepository {
	
	public Reservation makeReservation(ReservationDTO reserationDto);
	public Reservation cancelReservation(int reservationId);
	
}
