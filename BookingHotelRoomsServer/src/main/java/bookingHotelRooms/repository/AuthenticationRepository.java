package bookingHotelRooms.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import bookingHotelRooms.model.User;

@Repository
public interface AuthenticationRepository extends JpaRepository<User, Integer> {
	
	@Query("SELECT u FROM User u WHERE u.email = :email AND u.password = :password")
	public User signIn(@Param("email") String email, @Param("password") String password);
	
}
