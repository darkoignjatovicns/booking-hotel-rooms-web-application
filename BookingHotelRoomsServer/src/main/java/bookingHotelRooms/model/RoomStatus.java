package bookingHotelRooms.model;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the room_status database table.
 * 
 */
@Entity
@Table(name="room_status")
@NamedQuery(name="RoomStatus.findAll", query="SELECT r FROM RoomStatus r")
public class RoomStatus implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Column(name="room_status")
	private String roomStatus;

	//bi-directional many-to-one association to Room
	@OneToMany(mappedBy="roomStatus")
	@JsonIgnore
	private List<Room> rooms;

	public RoomStatus() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getRoomStatus() {
		return this.roomStatus;
	}

	public void setRoomStatus(String roomStatus) {
		this.roomStatus = roomStatus;
	}

	public List<Room> getRooms() {
		return this.rooms;
	}

	public void setRooms(List<Room> rooms) {
		this.rooms = rooms;
	}

	public Room addRoom(Room room) {
		getRooms().add(room);
		room.setRoomStatus(this);

		return room;
	}

	public Room removeRoom(Room room) {
		getRooms().remove(room);
		room.setRoomStatus(null);

		return room;
	}

}