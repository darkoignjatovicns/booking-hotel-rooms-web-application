package bookingHotelRooms.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;


/**
 * The persistent class for the room database table.
 * 
 */
@Entity
@NamedQuery(name="Room.findAll", query="SELECT r FROM Room r")
public class Room implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;

	@Lob
	@Column(name="about_room")
	private String aboutRoom;

	@Column(name="adults_max")
	private int adultsMax;

	@Column(name="children_max")
	private int childrenMax;

	@Temporal(TemporalType.DATE)
	@Column(name="created_date")
	private Date createdDate;

	@Lob
	private byte[] image;

	private String imagePut;

	@Temporal(TemporalType.DATE)
	@Column(name="last_update_date")
	private Date lastUpdateDate;

	@Column(name="price_per_night")
	private int pricePerNight;

	@Column(name="room_title")
	private String roomTitle;

	//bi-directional many-to-one association to Hotel
	@ManyToOne
	private Hotel hotel;

	//bi-directional many-to-one association to RoomStatus
	@ManyToOne
	@JoinColumn(name="room_status_id")
	private RoomStatus roomStatus;

	//bi-directional many-to-one association to Reservation
	@OneToMany(mappedBy="room")
	@JsonIgnore
	private List<Reservation> reservations;

	public Room() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAboutRoom() {
		return this.aboutRoom;
	}

	public void setAboutRoom(String aboutRoom) {
		this.aboutRoom = aboutRoom;
	}

	public int getAdultsMax() {
		return this.adultsMax;
	}

	public void setAdultsMax(int adultsMax) {
		this.adultsMax = adultsMax;
	}

	public int getChildrenMax() {
		return this.childrenMax;
	}

	public void setChildrenMax(int childrenMax) {
		this.childrenMax = childrenMax;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public byte[] getImage() {
		return this.image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getImagePut() {
		return this.imagePut;
	}

	public void setImagePut(String imagePut) {
		this.imagePut = imagePut;
	}

	public Date getLastUpdateDate() {
		return this.lastUpdateDate;
	}

	public void setLastUpdateDate(Date lastUpdateDate) {
		this.lastUpdateDate = lastUpdateDate;
	}

	public int getPricePerNight() {
		return this.pricePerNight;
	}

	public void setPricePerNight(int pricePerNight) {
		this.pricePerNight = pricePerNight;
	}

	public String getRoomTitle() {
		return this.roomTitle;
	}

	public void setRoomTitle(String roomTitle) {
		this.roomTitle = roomTitle;
	}

	public Hotel getHotel() {
		return this.hotel;
	}

	public void setHotel(Hotel hotel) {
		this.hotel = hotel;
	}

	public RoomStatus getRoomStatus() {
		return this.roomStatus;
	}

	public void setRoomStatus(RoomStatus roomStatus) {
		this.roomStatus = roomStatus;
	}

	public List<Reservation> getReservations() {
		return this.reservations;
	}

	public void setReservations(List<Reservation> reservations) {
		this.reservations = reservations;
	}

	public Reservation addReservation(Reservation reservation) {
		getReservations().add(reservation);
		reservation.setRoom(this);

		return reservation;
	}

	public Reservation removeReservation(Reservation reservation) {
		getReservations().remove(reservation);
		reservation.setRoom(null);

		return reservation;
	}

}